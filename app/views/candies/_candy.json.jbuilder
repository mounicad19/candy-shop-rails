json.extract! candy, :id, :name, :shelved, :created_at, :updated_at
json.url candy_url(candy, format: :json)
