json.extract! shelf, :id, :candies, :shop_id, :created_at, :updated_at
json.url shelf_url(shelf, format: :json)
