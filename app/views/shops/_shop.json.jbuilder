json.extract! shop, :id, :name, :shelves, :created_at, :updated_at
json.url shop_url(shop, format: :json)
