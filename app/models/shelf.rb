class Shelf < ApplicationRecord
    has_many :candies
    belongs_to :shop
end
