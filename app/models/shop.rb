class Shop < ApplicationRecord
    has_many :shelves
    has_many :candies, through: :shelves

    # def total_shelves
    # shelves.joins(:shelves).uniq.count
    # end
end
 