class CreateShops < ActiveRecord::Migration[6.1]
  def change
  # drop_table :shops
    create_table :shops do |t|
      t.string :name
      t.integer :shelves

      t.timestamps
    end
  end
end
