class AddShelfRefToCandies < ActiveRecord::Migration[6.1]
  def change
    add_reference :candies, :shelf, null: false, foreign_key: true
  end
end