class CreateCandies < ActiveRecord::Migration[6.1]
  
#   def change

#  end
  def change
    create_table :candies do |t|
      t.string :name
      t.boolean :shelved

      t.timestamps
    end
    # drop_table :candies 
  end

end
