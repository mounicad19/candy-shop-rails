class AddForeignKeyToShelf < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :shelves, :shops
  end
end
