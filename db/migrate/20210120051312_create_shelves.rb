class CreateShelves < ActiveRecord::Migration[6.1]
  def change
    create_table :shelves do |t|
      t.integer :candies
      t.string :shop_id

      t.timestamps
    end
  end
  # def change
  #   drop_table :shelves 
  # end
end
